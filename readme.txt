=== Future Reference - WebHooks Deploy Button ===
Contributors: analogmemory
Tags: admin, settings
Requires at least: 4.0
Tested up to: 4.9
Stable tag: 1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Creates a deploy button that sends a customizable WebHook
