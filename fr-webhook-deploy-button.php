<?php
/**
 * @package FR_Webhook_Deploy_Button
 * @since   1.1.0
 *
 * Plugin Name:       FR WebHook Deploy Button
 * Plugin URI:        https://gitlab.com/future-reference/fr-webhook-deploy-button/
 * Description:       Creates a deploy button that sends a customizable WebHook
 * Version:           1.1.0
 * Author:            Future Reference
 * Author URI:        https://futurereference.co/
 * License:           GPLv2 or later
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * GitLab Plugin URI: https://gitlab.com/future-reference/fr-webhook-deploy-button/
 */

/**
 * If this file is called directly, abort.
 *
 * @since 1.0.0
 */
 defined('ABSPATH') OR exit;

/**
 * Include the dependencies needed to instantiate the plugin.
 *
 * @since 1.0.0
 */
include_once(plugin_dir_path(__FILE__) . 'shared/class-deserializer.php');

foreach (glob(plugin_dir_path(__FILE__) . 'admin/*.php') as $file) {
		include_once $file;
}

/**
 * Entrypoint for the FR Webhook Deploy Button plugin
 *
 * @since 1.0.0
 */
add_action('plugins_loaded', 'fr_webhook_deploy_button');
function fr_webhook_deploy_button()
{
		$serializer = new FR_Webhook_Serializer();
		$serializer->init();

		$deserializer = new FR_Webhook_Deserializer();

		$plugin = new FR_Webhook_Submenu(new FR_Webhook_Submenu_Page($deserializer));
		$plugin->init();

		$webhook_button = new FR_Webhook_DeployButton($deserializer);
}
