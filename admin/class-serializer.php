<?php
/**
 * Performs all sanitization functions required to save the option values to
 * the database.
 *
 * @package FR_Webhook_Deploy_Button
 * @since   1.1.0
 */

/**
 * Performs all sanitization functions required to save the option values to
 * the database.
 *
 * This will also check the specified nonce and verify that the current user has
 * permission to save the data.
 *
 * @package FR_Webhook_Deploy_Button
 */
class FR_Webhook_Serializer
{
	public function init()
	{
		add_action('admin_post', array($this, 'save'));
	}

	public function save()
	{
		// First, validate the nonce and verify the user as permission to save.
		if (!($this->has_valid_nonce() && current_user_can('manage_options'))) {
			// TODO: Display an error message.
		}

		/* Webhook Settings */

		if (wp_unslash($_POST['webhook_url']) !== null) {
			update_option('fr_webhook_settings[webhook_url]', $_POST['webhook_url']);
		}

		if (wp_unslash($_POST['webhook_url_secondary']) !== null) {
			update_option('fr_webhook_settings[webhook_url_secondary]', $_POST['webhook_url_secondary']);
		}

		if (wp_unslash($_POST['webhook_arg_1_key']) !== null) {
			update_option('fr_webhook_settings[webhook_arg_1_key]', $_POST['webhook_arg_1_key']);
		}

		if (wp_unslash($_POST['webhook_arg_1_value']) !== null) {
			update_option('fr_webhook_settings[webhook_arg_1_value]', $_POST['webhook_arg_1_value']);
		}

		if (wp_unslash($_POST['webhook_arg_2_key']) !== null) {
			update_option('fr_webhook_settings[webhook_arg_2_key]', $_POST['webhook_arg_2_key']);
		}

		if (wp_unslash($_POST['webhook_arg_2_value']) !== null) {
			update_option('fr_webhook_settings[webhook_arg_2_value]', $_POST['webhook_arg_2_value']);
		}

		$this->redirect();
	}

	/**
	 * Determines if the nonce variable associated with the options page is set
	 * and is valid.
	 *
	 * @access private
	 * @return boolean False if the field isn't set or the nonce value is invalid;
	 *                 otherwise, true.
	 */
	private function has_valid_nonce()
	{
		// If the field isn't even in the $_POST, then it's invalid.
		if (!isset($_POST['fr_webhook_settings_nonce'])) {
			return false;
		}

		$field  = wp_unslash($_POST['fr_webhook_settings_nonce']);
		$action = 'fr_webhook_settings_save';

		return wp_verify_nonce($field, $action);
	}

	/**
	 * Redirect to the page from which we came (which should always be the
	 * admin page. If the referred isn't set, then we redirect the user to
	 * the login page.
	 *
	 * @access private
	 */
	private function redirect()
	{
		// To make the Coding Standards happy, we have to initialize this.
		if (!isset($_POST['_wp_http_referer'])) {
			$_POST['_wp_http_referer'] = wp_login_url();
		}

		// Sanitize the value of the $_POST collection for the Coding Standards.
		$url = sanitize_text_field(wp_unslash($_POST['_wp_http_referer']));

		// Redirect back to the admin page.
		wp_safe_redirect(urldecode($url));
		exit;
	}
}
