<div class="wrap">

	<div id="icon-options-general" class="icon32"></div>
	<h1><?php esc_attr_e(get_admin_page_title(), 'FR_Webhook_Deploy_Button'); ?></h1>

	<form method="post" action="<?php echo esc_html(admin_url('admin-post.php')); ?>">

		<div id="poststuff">
			<div id="post-body" class="metabox-holder columns-2">

							<!-- main content -->
				<div id="post-body-content">
					<div class="meta-box-sortables ui-sortable">

						<div class="postbox">

							<div class="handlediv" title="Click to toggle"><br></div>
							<h2 class="hndle">
								<span>
									<?php esc_attr_e('Webhook Settings', 'FR_Webhook_Deploy_Button'); ?>
								</span>
							</h2>

							<div class="inside">

								<p>
									<label for="">
										<b>Webhook URL</b> &nbsp;
										<input id="webhook_url"
											name="webhook_url"
											type="text"
											value="<?php echo $this->deserializer->get_value('fr_webhook_settings[webhook_url]'); ?>"
											class="regular-text" />
									</label>
								</p>
								<p>
									<label for="">
										<b>Secondary Webhook URL</b> &nbsp;
										<input id="webhook_url_secondary"
											name="webhook_url_secondary"
											type="text"
											value="<?php echo $this->deserializer->get_value('fr_webhook_settings[webhook_url_secondary]'); ?>"
											class="regular-text" />
									</label>
								</p>

								<h4>Optional Arguments</h4>

								<p>
									<label for="">
										<b>Argument 1</b> &nbsp;
										<input id="webhook_arg_1_key"
											name="webhook_arg_1_key"
											type="text"
											value="<?php echo esc_attr($this->deserializer->get_value('fr_webhook_settings[webhook_arg_1_key]')); ?>"
											placeholder="key"
											class="all-options" />
										<input id="webhook_arg_1_value"
											name="webhook_arg_1_value"
											type="text"
											placeholder="value"
											value="<?php echo esc_attr($this->deserializer->get_value('fr_webhook_settings[webhook_arg_1_value]')); ?>"
											class="all-options" />
									</label>
								</p>

								<p>
									<label for="">
										<b>Argument 2</b> &nbsp;
										<input id="webhook_arg_2_key"
											name="webhook_arg_2_key"
											type="text"
											placeholder="key"
											value="<?php echo esc_attr($this->deserializer->get_value('fr_webhook_settings[webhook_arg_2_key]')); ?>"
											class="all-options" />
										<input id="webhook_arg_2_value"
											name="webhook_arg_2_value"
											type="text"
											placeholder="value"
											value="<?php echo esc_attr($this->deserializer->get_value('fr_webhook_settings[webhook_arg_2_value]')); ?>"
											class="all-options" />
									</label>
								</p>

							</div>
						</div>

					</div>
					<!-- .meta-box-sortables .ui-sortable -->
				</div>
				<!-- post-body-content -->

				<!-- sidebar -->
				<div id="postbox-container-1" class="postbox-container">

					<div class="meta-box-sortables">

						<div class="postbox">

							<h2><?php esc_attr_e('Update Settings', 'FR_Webhook_Deploy_Button'); ?></h2>

							<div class="inside">
								<?php
									wp_nonce_field('fr_webhook_settings_save', 'fr_webhook_settings_nonce');
									submit_button();
								?>
							</div>
							<!-- .inside -->
						</div>
						<!-- .postbox -->
					</div>
					<!-- .meta-box-sortables -->
				</div>
				<!-- #postbox-container-1 .postbox-container -->

			</div>
		</div>

	</form>

</div>
