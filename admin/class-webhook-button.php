<?php
/**
 * Creates Deploy Button
 *
 * @package FR_Webhook_Deploy_Button
 * @since   1.1.0
 */

class FR_Webhook_DeployButton
{
	private $webhook_urls = [];
	private $webhook_arg_1_key;
	private $webhook_arg_1_value;
	private $webhook_arg_2_key;
	private $webhook_arg_2_value;

	public function __construct($deserializer)
	{
		$this->deserializer = $deserializer;

		$this->webhook_urls[] = $this->deserializer->get_value('fr_webhook_settings[webhook_url]');

		if ($this->deserializer->get_value('fr_webhook_settings[webhook_url_secondary]')) {
			$this->webhook_urls[] = $this->deserializer->get_value('fr_webhook_settings[webhook_url_secondary]');
		}

		$this->webhook_arg_1_key = $this->deserializer->get_value('fr_webhook_settings[webhook_arg_1_key]');
		$this->webhook_arg_1_value = $this->deserializer->get_value('fr_webhook_settings[webhook_arg_1_value]');

		$this->webhook_arg_2_key = $this->deserializer->get_value('fr_webhook_settings[webhook_arg_2_key]');
		$this->webhook_arg_2_value = $this->deserializer->get_value('fr_webhook_settings[webhook_arg_2_value]');

		add_action('admin_bar_menu', array($this, 'deploy_button_admin_bar_link'), 90);

		add_action('admin_head', array($this, 'deploy_button_style'), 90);

		add_action('init', array($this, 'process_deploy_request'));
	}

	public static function deploy_button_admin_bar_link ($wp_admin_bar) {
		$args = array(
			'id'     => 'deploy-site-button',
			'href'   => wp_nonce_url(add_query_arg('_deploy_content', 'deploy'), '_deploy_content_nonce'),
			'title'  => 'Deploy Content',
			'meta'   => array('title' => "This will gather all changes and rebuild static site. Takes ~1 minute to complete.")
		);
		$wp_admin_bar->add_menu($args);
	}

	public function deploy_button_style() {
		echo '<style>
			.wp-admin #wpadminbar #wp-admin-bar-deploy-site-button > .ab-item {
				color: #fff;
				background-color: #0073aa;
			}
			.wp-admin #wpadminbar #wp-admin-bar-deploy-site-button > .ab-item::before {
				content: "\f310";
				top: 2px;
				color: #fff;
			}
		</style>';
	}

	public function process_deploy_request ($data) {

		// check if clear request
		if (empty($_GET['_deploy_content']) || $_GET['_deploy_content'] !== 'deploy') {
				return;
		}

		// validate nonce
		if ( empty($_GET['_wpnonce']) OR ! wp_verify_nonce($_GET['_wpnonce'], '_deploy_content_nonce') ) {
				return;
		}

		wp_nonce_url(add_query_arg('_deploy_content', false), '_deploy_content_nonce');

		$this->webhook_deploy();
	}

	public function webhook_deploy() {
		// Allows for webhook arguments to be sent
		$args = array();
		if (!empty($this->webhook_arg_1_key) && !empty($this->webhook_arg_1_value)) {
			$args[$this->webhook_arg_1_key] = $this->webhook_arg_1_value;
		}
		if (!empty($this->webhook_arg_2_key) && !empty($this->webhook_arg_2_value)) {
			$args[$this->webhook_arg_2_key] = $this->webhook_arg_2_value;
		}

		foreach($this->webhook_urls as $url) {
			$c = curl_init($url);

			curl_setopt($c, CURLOPT_POSTFIELDS, json_encode($args));
			curl_setopt($c, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
			curl_setopt($c, CURLOPT_CUSTOMREQUEST, "POST");

			curl_exec($c);

			if(curl_error($c)) {
				add_action('admin_notices', array($this, 'webhook_deploy_error'));
			} else {
				add_action('admin_notices', array($this, 'webhook_deploy_notice'));
			}

			curl_close($c);
		}
		return;
	}

	public function webhook_deploy_notice() {
		echo sprintf(
				'<div class="notice notice-success is-dismissible"><p>%s</p></div>',
				'<b>Website content sent for deployment</b> &nbsp;&nbsp;<a href="' . home_url() . '" target="_blank">' . home_url() . '</a><br>This will gather all changes and rebuild static site. Takes ~1 minute to complete.'
		);
	}

	public function webhook_deploy_error() {
		echo sprintf(
				'<div class="notice notice-error is-dismissible"><p>%s</p></div>',
				esc_html__('Webhook settings not set or incorrect.', 'deploy-content')
		);
	}
}
